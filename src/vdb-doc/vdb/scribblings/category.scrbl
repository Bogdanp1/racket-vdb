;; This file is part of racket-vdb - Racket interface to Portage VDB.
;; Copyright (c) 2022-2023, Maciej Barć <xgqt@riseup.net>
;; Licensed under the GNU GPL v2 License
;; SPDX-License-Identifier: GPL-2.0-or-later

;; racket-vdb is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 2 of the License, or
;; (at your option) any later version.

;; racket-vdb is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with racket-vdb.  If not, see <https://www.gnu.org/licenses/>.


#lang scribble/manual

@(require (for-label racket
                     vdb/env
                     vdb/category))


@title[#:tag "vdb-category"]{Categories}


@defmodule[vdb/category]


@defproc[
 (vdb-list)
 (listof path?)
 ]{
 Return a list of all directories inside @racket[vdb-path].

 This is like a @racket[directory-list].
}

@defproc[
 (vdb-categories/path)
 (listof path?)
 ]{
 Return a list of all category paths inside @racket[vdb-path].

 @racket[vdb-categories/path] uses some mechanisms to ensure
 that the directory makes sense as a Portage package category.
}

@defproc[
 (vdb-categories)
 (listof string?)
 ]{
 Return a list of package categories inside @racket[vdb-path].
}

@defproc[
 (vdb-category-path [category string?])
 path?
 ]{
 Return a path to @racket[category] inside @racket[vdb-path].

 This is like appending @racket[category] to @racket[vdb-path].
}

@defproc[
 (vdb-category-exists? [category string?])
 boolean?
 ]{
 Check if given @racket[category] exists inside @racket[vdb-path].
 }

@defproc[
 (vdb-category-packages/path [category string?])
 (listof path?)
 ]{
 Return a list of all packages' paths inside of @racket[category] directory.
}

@defproc[
 (vdb-category-packages [category string?])
 (listof string?)
 ]{
 Return a list of all packages inside of @racket[category] directory.
}
