# VDB

<p align="center">
    <a href="https://archive.softwareheritage.org/browse/origin/?origin_url=https://gitlab.com/gentoo-racket/racket-vdb">
        <img src="https://archive.softwareheritage.org/badge/origin/https://gitlab.com/gentoo-racket/racket-vdb/">
    </a>
    <a href="https://gitlab.com/gentoo-racket/racket-vdb/pipelines">
        <img src="https://gitlab.com/gentoo-racket/racket-vdb/badges/master/pipeline.svg">
    </a>
</p>

Racket interface the to Portage VDB.


## About

Racket library to query the Portage's internal package database (VDB).


## Installation

### Make

Use GNU Make to install Racket-VDB from its project directory.

```sh
make install
```

### Raco

Use raco to install Racket-VDB from the official Racket Package Catalog.

```sh
raco pkg install --auto --skip-installed --user vdb
```

### Req

Use Req to install Racket-VDB from its project directory.

``` sh
raco req --everything --verbose
```


## Documentation

Documentation can either be built locally with `make public`
or browsed online on either
[GitLab pages](https://gentoo-racket.gitlab.io/racket-vdb/)
or [Racket-Lang Docs](https://docs.racket-lang.org/vdb/).


## License

Copyright (c) 2022-2023, Maciej Barć <xgqt@riseup.ne>

Licensed under the GNU GPL v2 License

SPDX-License-Identifier: GPL-2.0-or-later
