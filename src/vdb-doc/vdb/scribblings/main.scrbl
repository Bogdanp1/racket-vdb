;; This file is part of racket-vdb - Racket interface to Portage VDB.
;; Copyright (c) 2022-2023, Maciej Barć <xgqt@riseup.net>
;; Licensed under the GNU GPL v2 License
;; SPDX-License-Identifier: GPL-2.0-or-later

;; racket-vdb is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 2 of the License, or
;; (at your option) any later version.

;; racket-vdb is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with racket-vdb.  If not, see <https://www.gnu.org/licenses/>.


#lang scribble/manual

@(require ziptie/git/hash
          vdb/version)


@(define (in-upstream path)
   (format "https://gitlab.com/gentoo-racket/racket-vdb/-/tree/~a/" path))


@title[#:tag "vdb"]{VDB}

@author[@author+email["Maciej Barć" "xgqt@riseup.net"]]


Racket interface to Portage VDB.

Version: @link[@in-upstream[@VERSION]]{@VERSION},
commit hash:
@(let ([git-hash (git-get-hash #:short? #t)])
   (case git-hash
     [("N/A")
      (displayln "[WARNING] Not inside a git repository!")
      git-hash]
     [else
      (link (in-upstream git-hash) git-hash)]))


@table-of-contents[]

@include-section{env.scrbl}
@include-section{path.scrbl}
@include-section{category.scrbl}
@include-section{package.scrbl}
@include-section{information.scrbl}
@include-section{write.scrbl}
@include-section{version.scrbl}

@index-section[]
