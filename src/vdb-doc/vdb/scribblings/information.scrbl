;; This file is part of racket-vdb - Racket interface to Portage VDB.
;; Copyright (c) 2022-2023, Maciej Barć <xgqt@riseup.net>
;; Licensed under the GNU GPL v2 License
;; SPDX-License-Identifier: GPL-2.0-or-later

;; racket-vdb is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 2 of the License, or
;; (at your option) any later version.

;; racket-vdb is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with racket-vdb.  If not, see <https://www.gnu.org/licenses/>.


#lang scribble/manual

@(require (for-label racket
                     pmsf/depend
                     pmsf/iuse
                     pmsf/keywords
                     pmsf/slot
                     vdb/env
                     vdb/information
                     vdb/information/keys)
          (only-in threading ~>>)
          (only-in typed/pmsf/depend pdepend?)
          (only-in typed/pmsf/iuse piuses?)
          (only-in typed/pmsf/keywords pkeywords?)
          (only-in typed/pmsf/slot pslot?)
          vdb/information/keys)


@title[#:tag "vdb-information"]{Extracting information}


@defmodule[vdb/information]


@defproc[
 (vdb-package-raw-info [package-full-name string?])
 hash?
 ]{
 Return all data of @racket[package-full-name] known to VDB, unparsed.
}

@defproc[
 (vdb-raw-info->canonical [raw-metadata hash?])
 hash?
 ]{
 Parse raw VDB package data.
}

@defproc[
 (vdb-package-info [package-full-name string?])
 hash?
 ]{
 Return all data of @racket[package-full-name] known to VDB,
 as parsed by @racket[vdb-raw-info->canonical].
}

@defproc[
 (vdb-package-info-ref [package-full-name string?] [key symbol?])
 any
 ]{
 Return a value of @racket[key] extracted from the package's VDB data.
}

@defproc[
 (vdb-package-ebuild [package-full-name string?])
 (listof string?)
 ]{
 Return a ebuild script
 (cut into lines)
 extracted from the package's VDB data.
}

@defproc[
 (vdb-package-environment [package-full-name string?])
 (listof string?)
 ]{
 Return the bash environment that was used during the package build
 (cut into lines).
}

@defproc[
 (vdb-package-environment-variables [package-full-name string?])
 environment-variables?
 ]{
 Return the environment variables that were declared during the package build.
}


@section[#:tag "vdb-information-keys"]{Supported keys}

@defmodule[vdb/information/keys]

@defthing[
 vdb-info-canonical-keys
 (listof symbol?)
 ]{
 List of all currently supported info canonical keys.

 Currently the following associations of key to data types are defined:
 @(apply itemlist
         (for/list ([pair (~>> vdb-info-canonical-assoc
                               hash->list
                               (sort _ symbol<? #:key car))])
           (let ([k
                  (car pair)]
                 [v
                  (cdr pair)])
             (item (index (format "~a key" k)
                          (code (format "~v" k))
                          " --- "
                          (cond
                            [(equal? k 'CONTENTS)
                             (racket (hash/c symbol? content?))]
                            [(equal? v boolean?)
                             (racket boolean?)]
                            [(equal? v exact-nonnegative-integer?)
                             (racket exact-nonnegative-integer?)]
                            [(equal? v hash?)
                             (racket (hash/c symbol? (listof string?)))]
                            [(equal? v list?)
                             (racket (listof string?))]
                            [(equal? v pdepend?)
                             (racket pdepend?)]
                            [(equal? v piuses?)
                             (racket piuses?)]
                            [(equal? v pkeywords?)
                             (racket pkeywords?)]
                            [(equal? v pslot?)
                             (racket pslot?)]
                            [else  ; (equal? v string?)
                             (racket string?)]))))))
}
