;; This file is part of racket-vdb - Racket interface to Portage VDB.
;; Copyright (c) 2022-2023, Maciej Barć <xgqt@riseup.net>
;; Licensed under the GNU GPL v2 License
;; SPDX-License-Identifier: GPL-2.0-or-later

;; racket-vdb is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 2 of the License, or
;; (at your option) any later version.

;; racket-vdb is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with racket-vdb.  If not, see <https://www.gnu.org/licenses/>.


#lang typed/racket/base

(require threading
         upi/basename
         upi/dirname
         "category.rkt"
         "env.rkt")

(require/typed file/glob
  [glob (-> Path (Listof Path))])

(provide (all-defined-out))


(define (path->package-full-name [package-path : Path-String]) : String
  (string-append (~>> package-path dirname basename)
                 "/"
                 (basename package-path)))


(define (vdb-packages-paths) : (Listof Path)
  (~>> (vdb-categories)
       (map vdb-category-packages/path)
       (apply append)))

(define (vdb-packages) : (Listof String)
  (map path->package-full-name (vdb-packages-paths)))


#|
With no arguments this is equal to "vdb-packages-paths".
Examples:
(vdb-find-packages "app-portage" "e*")
|#

(define (vdb-find-packages/path [package-glob : String]) : (Listof Path)
  (~>> package-glob
       (build-path (vdb-path))
       glob))

(define (vdb-find-packages [package-glob : String]) : (Listof String)
  (map path->package-full-name
       (vdb-find-packages/path package-glob)))
