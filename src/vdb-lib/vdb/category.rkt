;; This file is part of racket-vdb - Racket interface to Portage VDB.
;; Copyright (c) 2022-2023, Maciej Barć <xgqt@riseup.net>
;; Licensed under the GNU GPL v2 License
;; SPDX-License-Identifier: GPL-2.0-or-later

;; racket-vdb is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 2 of the License, or
;; (at your option) any later version.

;; racket-vdb is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with racket-vdb.  If not, see <https://www.gnu.org/licenses/>.


#lang typed/racket/base

(require threading
         upi/basename
         "env.rkt"
         "support/dir-list.rkt")

(provide (all-defined-out))


(define (vdb-list) : (Listof Path)
  (vdb-guard)
  (dir-list (vdb-path)))

(define (vdb-categories/path) : (Listof Path)
  (for/list ([file-path : Path (vdb-list)]
             #:when (directory-exists? file-path)
             #:when (~>> file-path
                         dir-list
                         (filter directory-exists?)
                         null?
                         not))
    file-path))

(define (vdb-categories) : (Listof String)
  (map basename (vdb-categories/path)))

(define (vdb-category-path [category : String]) : Path
  (build-path (vdb-path) category))

(define (vdb-category-exists? [category : String]) : Boolean
  (directory-exists? (vdb-category-path category)))

(define-syntax-rule (vdb-category-guard category)
  (unless (vdb-category-exists? category)
    (error 'vdb-category-guard
           "the category ~v does not exist in the system Portage VDB"
           category)))

(define (vdb-category-packages/path [category : String]) : (Listof Path)
  (vdb-category-guard category)
  (~>> category
       vdb-category-path
       dir-list
       (filter directory-exists?)))

(define (vdb-category-packages [category : String]) : (Listof String)
  (map basename (vdb-category-packages/path category)))
