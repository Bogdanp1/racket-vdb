;; This file is part of racket-vdb - Racket interface to Portage VDB.
;; Copyright (c) 2022-2023, Maciej Barć <xgqt@riseup.net>
;; Licensed under the GNU GPL v2 License
;; SPDX-License-Identifier: GPL-2.0-or-later

;; racket-vdb is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 2 of the License, or
;; (at your option) any later version.

;; racket-vdb is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with racket-vdb.  If not, see <https://www.gnu.org/licenses/>.


#lang racket/base


(module+ test
  (require rackunit
           (only-in threading ~>>)
           vdb)

  (when (vdb-available?)
    (test-not-exn "Parse top VDB package"
                  (lambda ()
                    (~>> (vdb-packages)
                         car
                         vdb-package-info
                         void)))))
