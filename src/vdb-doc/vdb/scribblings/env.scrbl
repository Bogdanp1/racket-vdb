;; This file is part of racket-vdb - Racket interface to Portage VDB.
;; Copyright (c) 2022-2023, Maciej Barć <xgqt@riseup.net>
;; Licensed under the GNU GPL v2 License
;; SPDX-License-Identifier: GPL-2.0-or-later

;; racket-vdb is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 2 of the License, or
;; (at your option) any later version.

;; racket-vdb is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with racket-vdb.  If not, see <https://www.gnu.org/licenses/>.


#lang scribble/manual

@(require (for-label racket
                     vdb/env))


@title[#:tag "vdb-env"]{Environment}


@defmodule[vdb/env]


@defthing[
 EROOT
 path?
 ]{
 The value of @racket{EROOT} environment variable,
 if unset it is equal to @racket{/}.
}


@defparam[
 vdb-path
 directory-path
 path-string?
 #:value (get-default-vdb-path)
 ]{
 Path to the current system's Portage VDB.

 Usually it will be equal to @racket{/var/db/pkg}.
}


@defproc[
 (get-default-vdb-path)
 path?
 ]{
 Return the default Portage VDB path of the current system.
}

@defproc[
 (vdb-available?)
 boolean?
 ]{
 Check if the Portage VDB is available.
}
